CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers
* Previous Maintainers

INTRODUCTION
------------

This module provides support for authenticating to a Vault server
using the AppRole strategy.

* For the full description of the module visit:
  https://www.drupal.org/project/vault_auth_approle

* To submit bug reports and feature suggestions, or to track changes visit:
  https://www.drupal.org/project/issues/vault_auth_approle

* See https://learn.hashicorp.com/tutorials/vault/approle for more information
  on the AppRole authentication strategy.

REQUIREMENTS
------------

This module requires the following modules:

* [Vault](https://www.drupal.org/project/issues/vault)
* [Key](https://www.drupal.org/project/key)

INSTALLATION
------------

Install the Vault Auth AppRole module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

CONFIGURATION
-------------

1. Navigate to admin/config/system/keys/add to create a key to store the 
   SecretId.

   The key should be of type Authentication and should provide the value of 
   the AppRole SecretId.

3. Proceed with configuring the Vault module at admin/config/system/vault as 
   documented in the Vault user guide. 

MAINTAINERS
-----------

* Conrad Lara - https://www.drupal.org/u/cmlara

PREVIOUS MAINTAINERS
--------------------
Nick Santamaria - https://www.drupal.org/u/nicksanta
