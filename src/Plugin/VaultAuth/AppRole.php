<?php

namespace Drupal\vault_auth_approle\Plugin\VaultAuth;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\key\KeyRepositoryInterface;
use Drupal\vault\Plugin\VaultAuthBase;
use Drupal\vault\Plugin\VaultPluginFormInterface;
use Vault\AuthenticationStrategies\AppRoleAuthenticationStrategy;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a approle-based authentication strategy for the vault client.
 *
 * @VaultAuth(
 *   id = "approle",
 *   label = "AppRole",
 *   description = @Translation("This authentication strategy uses an approle id and secret."),
 * )
 */
final class AppRole extends VaultAuthBase implements ContainerFactoryPluginInterface, VaultPluginFormInterface, ConfigurableInterface {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * The AppRole role ID.
   *
   * @var string
   */
  protected string $roleId;

  /**
   * The AppRole role secret.
   *
   * @var string
   */
  protected string $secretId;

  /**
   * Construct a new AppRole auth plugin instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\key\KeyRepositoryInterface $key_repository
   *   The key repository service.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   On error constructing plugin.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, TranslationInterface $string_translation, KeyRepositoryInterface $key_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->setStringTranslation($string_translation);

    if (empty($configuration['role_id']) || empty($configuration['secret_key_id'])) {
      // role_id or secret_id is not yet configured.
      // set to empty to allow initial config form usage.
      $this->setRoleId('');
      $this->setSecretId('');
      return;
    }
    $this->setRoleId($configuration['role_id']);

    $key_entity = $key_repository->getKey($configuration['secret_key_id']);
    if ($key_entity == NULL) {
      throw new PluginException('Secret Key does not exist');
    }

    $secret = $key_entity->getKeyValue();
    $this->setSecretId($secret);

  }

  /**
   * Sets $roleId property.
   *
   * @param string $roleId
   *   The roleId to set.
   *
   * @return self
   *   Current object.
   */
  public function setRoleId(string $roleId): AppRole {
    $this->roleId = $roleId;
    return $this;
  }

  /**
   * Gets $roleId property.
   *
   * @return string
   *   AppRole ID.
   */
  public function getRoleId(): string {
    return $this->roleId;
  }

  /**
   * Sets $secretId property.
   *
   * @param string $secretId
   *   the secretId to set.
   *
   * @return self
   *   Current object.
   */
  public function setSecretId(string $secretId): AppRole {
    $this->secretId = $secretId;
    return $this;
  }

  /**
   * Gets $secretId property.
   *
   * @return string
   *   AppRole secret.
   */
  public function getSecretId(): string {
    return $this->secretId;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('string_translation'),
      $container->get('key.repository')
    );

  }

  /**
   * {@inheritdoc}
   */
  public function getAuthenticationStrategy(): AppRoleAuthenticationStrategy {
    $authStrategy = new AppRoleAuthenticationStrategy($this->getRoleId(), $this->getSecretId());
    return $authStrategy;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $config = $this->getConfiguration();
    $form['role_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AppRole ID'),
      '#default_value' => $config['role_id'] ?? NULL,
    ];
    $form['secret_key_id'] = [
      '#type' => 'key_select',
      '#title' => $this->t('AppRole Secret'),
      '#key_filters' => ['type' => 'authentication'],
      '#default_value' => $config['secret_key_id'] ?? NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    // No additional validation required.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'role_id' => NULL,
      'secret_key_id' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    return [];
  }

}
