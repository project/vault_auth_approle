<?php

namespace Drupal\Tests\vault_auth_approle\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests Vault configuration form when using vault_auth_approle.
 *
 * @group vault_auth_approle
 * @codeCoverageIgnore
 */
class VaultAuthApproleConfigFormTest extends BrowserTestBase {

  /**
   * A user with administration access.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['vault', 'vault_auth_approle', 'key'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $user = $this->drupalCreateUser([
      'administer vault',
    ]);
    if (!$user) {
      $this->fail("Unable to create adminUser");
    }
    $this->adminUser = $user;
    $this->drupalLogin($this->adminUser);

    $key_config_secret_id = [
      'status' => TRUE,
      'id' => 'vault_ci_secret_id',
      'label' => 'Vault CI Test SecretId Key',
      'key_type' => 'authentication',
      'key_provider' => 'config',
      'key_provider_settings' => [
        'key_value' => 'Mocked secretId value',
      ],
      'key_input' => 'text_field',
    ];
    $this->config('key.key.vault_ci_secret_id')->setData($key_config_secret_id)->save(TRUE);
  }

  /**
   * Test the Vault config form using approle plugin.
   */
  public function testVaultConfigurationForm(): void {

    $edit['base_url'] = 'http://vault:8200';
    $edit['plugin_auth'] = 'approle';
    $this->drupalGet('admin/config/system/vault');
    // Save the auth method and reload the form.
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->statusCodeEquals(200);
    $edit['plugin_auth_settings[role_id]'] = 'Mock roleId String';
    $edit['plugin_auth_settings[secret_key_id]'] = 'vault_ci_secret_id';
    // Submit the form with the role_id and secret selected.
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->statusCodeEquals(200);
    $vault_config = $this->config('vault.settings')->get();
    assert(is_array($vault_config));
    $this->assertEquals('approle', $vault_config['plugin_auth']);
    $this->assertEquals('Mock roleId String', $vault_config['auth_plugin_config']['role_id']);
    $this->assertEquals('vault_ci_secret_id', $vault_config['auth_plugin_config']['secret_key_id']);
  }

}
