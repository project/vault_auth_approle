<?php

namespace Drupal\Tests\vault_auth_approle\Unit;

use Drupal\Core\Form\FormState;
use Drupal\key\Entity\Key;
use Drupal\key\KeyRepositoryInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\vault\Plugin\VaultAuthInterface;
use Drupal\vault_auth_approle\Plugin\VaultAuth\AppRole;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Vault\AuthenticationStrategies\AppRoleAuthenticationStrategy;

/**
 * Tests the Vault Auth Approle plugin.
 *
 * @group vault_auth_approle
 *
 * @covers \Drupal\vault_auth_approle\Plugin\VaultAuth\AppRole
 * @codeCoverageIgnore
 */
class ApprolePluginTest extends UnitTestCase {

  /**
   * Key Repository service mock.
   *
   * @var \Drupal\key\KeyRepositoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $keyRepoMock;

  /**
   * Default Plugin configuration.
   *
   * @var string[]
   */
  protected array $pluginConfig;

  /**
   * The configured VaultAuth Approle plugin.
   *
   * @var \Drupal\vault_auth_approle\Plugin\VaultAuth\AppRole
   */
  protected AppRole $plugin;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $key_entity_mock = $this->createMock(Key::class);
    $key_entity_mock->method('getKeyValue')
      ->willReturn('my_approle_secret_id');

    $return_map = [
      ['vault_auth_approle_id', $key_entity_mock],
      ['invalid_key_id', NULL],
    ];
    $this->keyRepoMock = $this->createMock(KeyRepositoryInterface::class);
    $this->keyRepoMock->method('getKey')
      ->willReturnMap($return_map);

    $this->pluginConfig = [
      'role_id' => 'test_role_id',
      'secret_key_id' => 'vault_auth_approle_id',
    ];

    $this->plugin = new AppRole($this->pluginConfig, 'approle', [], $this->getStringTranslationStub(), $this->keyRepoMock);

  }

  /**
   * Test the plugin create() method.
   */
  public function testCreate(): void {

    $service_map = [
      [
        'key.repository',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $this->keyRepoMock,
      ],
      [
        'string_translation',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $this->getStringTranslationStub(),
      ],
    ];

    $container_mock = $this->createMock(ContainerInterface::class);
    $container_mock->method('get')
      ->willReturnMap($service_map);

    $plugin = AppRole::create($container_mock, $this->pluginConfig, 'approle', []);
    $this->assertInstanceOf(AppRole::class, $plugin, 'Create returns AppRole plugin');
  }

  /**
   * Test the Approle plugin with no config.
   *
   * Necessary for form first load.
   */
  public function testObtainPluginNoConfig(): void {
    $this->plugin = new AppRole([], 'approle', [], $this->getStringTranslationStub(), $this->keyRepoMock);
    $this->assertInstanceOf(VaultAuthInterface::class, $this->plugin, 'Approle plugin returned with no config set');
  }

  /**
   * Test the Approle plugin can be constructed.
   */
  public function testObtainPlugin(): void {
    $this->assertInstanceOf(VaultAuthInterface::class, $this->plugin, 'Approle Plugin returned');
    $this->assertEquals(new AppRoleAuthenticationStrategy('test_role_id', 'my_approle_secret_id'), $this->plugin->getAuthenticationStrategy(), "Appprole strategy returned");
  }

  /**
   * Test the Approle plugin without role_id.
   *
   * Initial config form load will not have a role_id set.
   */
  public function testObtainPluginWithoutRoleId(): void {
    $test_config = [
      'secret_key_id' => 'vault_auth_approle_id',
    ];
    $this->plugin = new AppRole($test_config, 'approle', [], $this->getStringTranslationStub(), $this->keyRepoMock);
    $this->assertInstanceOf(VaultAuthInterface::class, $this->plugin, 'Approle plugin returned with no role_id set');
  }

  /**
   * Test the Approle plugin invalid Key.
   */
  public function testObtainPluginWithInvalidKey(): void {
    $test_config = [
      'role_id' => 'test_role_id',
      'secret_key_id' => 'invalid_key_id',
    ];
    $this->expectExceptionMessage('Secret Key does not exist');
    $this->plugin = new AppRole($test_config, 'approle', [], $this->getStringTranslationStub(), $this->keyRepoMock);
  }

  /**
   * Test the Approle plugin without providing a secret key id.
   *
   * Initial config form load will not have a secret key set.
   */
  public function testObtainPluginWithoutSecretKeyId(): void {
    $test_config = [
      'role_id' => 'test_role_id',
    ];
    $this->plugin = new AppRole($test_config, 'approle', [], $this->getStringTranslationStub(), $this->keyRepoMock);
    $this->assertInstanceOf(VaultAuthInterface::class, $this->plugin, 'Approle plugin returned with no role_id set');
  }

  /**
   * Test obtain configuration from plugin.
   */
  public function testGetConfiguration(): void {
    $this->assertEquals($this->pluginConfig, $this->plugin->getConfiguration(), 'Plugin returns correct config');
  }

  /**
   * Test obtain the default configuration from plugin.
   */
  public function testDefaultConfiguration(): void {
    $this->assertIsArray($this->plugin->defaultConfiguration(), 'Default Config returns array');
  }

  /**
   * Test set the configuration for plugin.
   */
  public function testSetConfiguration(): void {
    $test_config = [
      'role_id' => 'test_role_id',
      'secret_key_id' => 'invalid_key_id',
    ];
    $this->plugin->setConfiguration($test_config);
    $this->assertEquals($test_config, $this->plugin->getConfiguration());
  }

  /**
   * Test set the configuration for plugin.
   */
  public function testCalculateDependencies(): void {
    $this->assertEquals([], $this->plugin->calculateDependencies());
  }

  /**
   * Test the configuration form build.
   */
  public function testBuildConfigurationForm(): void {
    $form_state = new FormState();
    $this->assertIsArray($this->plugin->buildConfigurationForm([], $form_state));
  }

  /**
   * Test config validation.
   *
   * This is a noop.
   *
   * @doesNotPerformAssertions
   */
  public function testValidateConfigurationForm(): void {
    $form_state = new FormState();
    $form = $this->plugin->buildConfigurationForm([], $form_state);
    $this->plugin->validateConfigurationForm($form, $form_state);
  }

  /**
   * Test config submission.
   */
  public function testSubmitConfigurationForm(): void {
    $form_state = new FormState();
    $form = $this->plugin->buildConfigurationForm([], $form_state);
    $form_state->setValue('role_id', 'test_role_id');
    $form_state->setValue('secret_key_id', 'invalid_key_id');

    $expected_config = [
      'role_id' => 'test_role_id',
      'secret_key_id' => 'invalid_key_id',
    ];
    $this->plugin->submitConfigurationForm($form, $form_state);
    $this->assertEquals($expected_config, $this->plugin->getConfiguration());
  }

}
